module Control(
	input [4:0]OPCODE,
	input cout, zero,
	output reg memRead, memWrite, memToReg, ALUSrc, RegWrite, RegDst, push, pop,
	output reg[1:0] AddrSrc, ALUOP
);
	always @(OPCODE) begin
		{memRead, memWrite, memToReg, ALUSrc, RegWrite, RegDst, push, pop} = 8'b0;
		{AddrSrc, ALUOP} = 4'b0;
		if(OPCODE[4:3] == 2'b00) begin
			ALUOP = 2'b00;
			RegWrite = 1'b1;
			AddrSrc = 2'b00;
		end
		else if(OPCODE[4:3] == 2'b01) begin
			ALUOP = 2'b00;
			ALUSrc = 1'b1;
			RegWrite = 1'b1;
			AddrSrc = 2'b00;
		end
		else if(OPCODE[4:2] == 3'b110) begin
			ALUOP = 2'b01;
			ALUSrc = 1'b1;
			RegWrite = 1'b1;
			AddrSrc = 2'b00;
		end
		else if(OPCODE[4:2] == 3'b100) begin
			case(OPCODE[1:0])
				0: begin
						ALUOP = 2'b10;
						ALUSrc = 1'b1;
						memRead = 1'b1;
						memToReg = 1'b1;
						RegWrite = 1'b1;
						AddrSrc = 2'b00;
				   end
				1: begin
						ALUOP = 2'b10;
						ALUSrc = 1'b1;
						memWrite = 1'b1;
						AddrSrc = 2'b00;
				   end
			endcase
		end
		else if(OPCODE[4:2] == 3'b101) begin
			case(OPCODE[1:0])
				0: begin
						if(zero)
							AddrSrc = 2'b01;
						else
							AddrSrc = 2'b00;
				   end
				1: begin
						if(!zero)
							AddrSrc = 2'b01;
						else
							AddrSrc = 2'b00;
				   end
				2: begin
						if(cout)
							AddrSrc = 2'b01;
						else
							AddrSrc = 2'b00;
				   end
				3: begin
						if(!cout)
							AddrSrc = 2'b01;
						else
							AddrSrc = 2'b00;
				   end
			endcase
		end
		else if(OPCODE == 5'b11100) begin
			AddrSrc = 2'b10;
		end
		else if(OPCODE == 5'b11101) begin
			AddrSrc = 2'b10;
			push = 1'b1;
		end
		else if(OPCODE == 5'b11110) begin
			AddrSrc = 2'b11;
			pop = 1'b1;
		end
	end
endmodule