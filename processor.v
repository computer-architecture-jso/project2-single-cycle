module processor(
	input clk, rst
);
	
	wire regWrite, aluSrc, memWrite, memRead, memToReg, push, pop, regDst, ld, zeroOut, coutOut;
	wire [1:0] addrSrc;
	wire [2:0] fn;
	wire [3:0] aluOperation;
	wire [4:0] opCode;

	dp d(clk, rst, regWrite, aluSrc, memWrite, memRead, memToReg, push, pop, regDst, addrSrc, ld, aluOperation, zeroOut, coutOut, fn, opCode);
	cu c(clk, zeroOut, coutOut, fn, opCode, memRead, memWrite, memToReg, aluSrc, regWrite, regDst, push, pop, ld, addrSrc, aluOperation);
	
endmodule

module FINALTB();
 	reg clk, rst;
  
  	processor p(clk, rst);
  	initial begin
	  	clk=1;
    	repeat (400)
    	#200 clk=~clk;
	end
	
	initial begin
	  	rst = 1;
	  	#200;
	  	rst = 0;
	  	#400;
	end
endmodule
