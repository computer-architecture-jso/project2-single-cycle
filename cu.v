module cu(
	input clk,
	input zero, cout,
	input [2:0] fn,
	input [4:0] opCode,
	output memRead, memWrite, memToReg, aluSrc, regWrite, regDst, push, pop, ld,
	output [1:0] addrSrc,
	output [3:0] aluOperation
);
	
	wire [1:0] aluOp;

	Control control(opCode, cout, zero, memRead, memWrite, memToReg, aluSrc, regWrite, regDst, push, pop, addrSrc, aluOp);
	ALUControl alucontrol(aluOp, fn, aluOperation, ld);

endmodule