module dp(
	input clk, rst,
	input regWrite, aluSrc, memWrite, memRead, memToReg, push, pop, regDst,
	input [1:0] addrSrc,
	input ld,
	input [3:0] aluOperation,
	output zeroOut, coutOut,
	output [2:0] fn,
	output [4:0] opCode
);
	
	wire [18:0] instruction;
	wire [11:0] pc_out, pc_one_out, stack_out, pc_branch_out, se_out, mux_to_pc;
	wire [7:0] read_data_one, read_data_two, mux_to_alu, alu_result, mem_data, mux_to_reg;
	wire [2:0] mux_to_reg_read;
	wire cout, zero;

	
	PC pc(clk, rst, mux_to_pc, pc_out);
	Adder12Bit add121(12'b000000000001, pc_out, pc_one_out);
	instructionMemory im(pc_out, instruction);
	MUX3Bit m1(instruction[7:5], instruction[13:11], regDst, mux_to_reg_read);
	registerFile rf(instruction[10:8], mux_to_reg_read, instruction[13:11], clk, rst, regWrite, mux_to_reg, read_data_one, read_data_two);
	stack s(clk, rst, push, pop, pc_one_out, stack_out);
	MUX8Bit m2(read_data_two, instruction[7:0], aluSrc, mux_to_alu);
	SE se(instruction[7:0], se_out);
	ALU alu(read_data_one, mux_to_alu, aluOperation, coutOut, alu_result, cout, zero);
	FlipFlop ff1(cout, clk, rst, ld, coutOut);
	FlipFlop ff2(zero, clk, rst, ld, zeroOut);
	Adder12BitSigned add122(pc_one_out, se_out, pc_branch_out);
	dataMemory dm(alu_result, read_data_two, memWrite, memRead, clk, rst, mem_data);
	MUX12Bit m3(pc_one_out, pc_branch_out, se_out, stack_out, addrSrc, mux_to_pc);
	MUX8Bit m4(alu_result, mem_data, memToReg, mux_to_reg);

	assign opCode = instruction[18:14];
	assign fn = instruction[16:14];

endmodule